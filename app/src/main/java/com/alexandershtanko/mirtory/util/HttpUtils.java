package com.alexandershtanko.mirtory.util;

import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okio.BufferedSink;
import okio.Okio;

public class HttpUtils {

    private static final String TAG = "HttpUtils";

    public static List makeRequest(String url, Map<String, String> parameters) throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url+"?"+createQueryString(parameters)).get()
                .build();
        Response response = client.newCall(request).execute();
        String responseJson = response.body().string();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS,true);

        return  objectMapper.readValue(responseJson, List.class);
    }


    public static void downloadFile(String url, Map<String, String> parameters, File outputFile) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url+"?"+createQueryString(parameters))
                .get()
                .build();
        Response response = client.newCall(request).execute();

        BufferedSink sink = Okio.buffer(Okio.sink(outputFile));

        sink.writeAll(response.body().source());
        sink.close();
    }

    public static JavaType getListResponseType(Class typeClass) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, typeClass);
    }

    private static RequestBody createRequestBodyFromMap(Map<String, String> parameters) {
        FormEncodingBuilder builder = new FormEncodingBuilder();
        if (parameters != null)
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                builder.add(entry.getKey(), entry.getValue());
            }
        return builder.build();
    }

    private static String createQueryString(Map<String,String> map) {
        StringBuilder sb = new StringBuilder();

        try {
            for (Map.Entry<String, String> e : map.entrySet()) {
                if (sb.length() > 0) {
                    sb.append('&');
                }

                sb.append(URLEncoder.encode(e.getKey(), "UTF-8")).append('=').append(URLEncoder.encode(e.getValue(), "UTF-8"));

            }
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG,"",e);
        }
        return sb.toString();
    }
}
