package com.alexandershtanko.mirtory.util;

import android.content.Context;
import android.os.Environment;

/**
 * Created by aleksandr on 02.08.15.
 */
public class AppUtils {
    public static String getDataDir(final Context context) throws Exception {
        return Environment.getExternalStorageDirectory().getAbsolutePath()+ "/pdf";//context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.dataDir;
    }
}
