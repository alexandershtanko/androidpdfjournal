package com.alexandershtanko.mirtory.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.widget.TextView;

import com.alexandershtanko.mirtory.R;

/**
 * Created by aleksandr on 11.06.17.
 */

public class AboutUsActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_about_us);

        TextView text = (TextView) findViewById(R.id.text);
        text.setText(Html.fromHtml(getResources().getString(R.string.text_about_us)));
    }
}
