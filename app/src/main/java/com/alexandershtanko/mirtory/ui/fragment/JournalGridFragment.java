package com.alexandershtanko.mirtory.ui.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alexandershtanko.mirtory.R;
import com.alexandershtanko.mirtory.exception.JournalException;
import com.alexandershtanko.mirtory.helper.JournalHelper;
import com.alexandershtanko.mirtory.listener.OnCardMenuItemClickListener;
import com.alexandershtanko.mirtory.listener.OnItemClickListener;
import com.alexandershtanko.mirtory.listener.OnPaymentInitListener;
import com.alexandershtanko.mirtory.listener.OnPaymentListener;
import com.alexandershtanko.mirtory.model.Journal;
import com.alexandershtanko.mirtory.model.OnRestoreListener;
import com.alexandershtanko.mirtory.ui.activity.AboutUsActivity;
import com.alexandershtanko.mirtory.ui.activity.MainActivity;
import com.alexandershtanko.mirtory.ui.adapter.JournalGridAdapter;
import com.alexandershtanko.mupdf.MuPDFActivity;

import java.util.Collections;
import java.util.List;


/**
 * Created by aleksandr on 31.07.15.
 */
public class JournalGridFragment extends Fragment implements OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, OnCardMenuItemClickListener {
    private static final String TAG = "JournalGridFragment";
    public static final int EXTERNAL_STORAGE_REQUEST_CODE_FOR_DOWNLOAD = 150;
    public static final int EXTERNAL_STORAGE_REQUEST_CODE_FOR_OPEN = 151;

    JournalHelper journalHelper;
    private MainActivity activity;
    private JournalGridAdapter adapter;
    private RecyclerView rvJournals;
    private SwipeRefreshLayout srLayout;
    private GridLayoutManager gridLayoutManager;
    private Journal lastJournal;
    private OnPaymentInitListener onPaymentInitListener = new OnPaymentInitListener() {
        @Override
        public void onSuccess() {

            Log.e(TAG, "On Payments init success");

            updateJournals();
        }

        @Override
        public void onError(String msg) {
            Log.e(TAG, msg);
            updateJournals();
        }
    };
    private Toolbar toolbar;

    public static Fragment newInstance() {
        return new JournalGridFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        journalHelper = activity.journalHelper;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_journal_grid, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    private void initView(final View view) {
        try {
            rvJournals = (RecyclerView) view.findViewById(R.id.rv_journals);
            toolbar = (Toolbar) view.findViewById(R.id.toolbar);
            srLayout = (SwipeRefreshLayout) view.findViewById(R.id.sr_layout);
            final TextView title = (TextView) view.findViewById(R.id.title);
            final ImageView menuButton = (ImageView) view.findViewById(R.id.button_menu);

            AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.appbar_layout);
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                boolean isShow = false;
                int scrollRange = -1;

                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.getTotalScrollRange();
                    }
                    if (scrollRange + verticalOffset == 0) {
                        title.setVisibility(View.VISIBLE);
                        menuButton.setColorFilter(view.getResources().getColor(R.color.white));
                        isShow = true;
                    } else if (isShow) {
                        title.setVisibility(View.INVISIBLE);
                        menuButton.setColorFilter(view.getResources().getColor(R.color.primary_dark));

                        isShow = false;
                    }
                }
            });


            NavigationView navigationView = (NavigationView) view.findViewById(R.id.navigation_view);
            final DrawerLayout drawerLayout = (DrawerLayout) view.findViewById(R.id.drawer_layout);
            final Context context = view.getContext();

            menuButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            });
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {

                    drawerLayout.closeDrawers();

                    switch (menuItem.getItemId()) {

                        case R.id.nav_site: {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mirtory.com"));
                            startActivity(intent);

                            return true;
                        }
                        case R.id.nav_privacy: {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mirtory.com/privacy_policy.html"));
                            startActivity(intent);

                            return true;
                        }
                        case R.id.nav_about: {
                            Intent intent = new Intent(context, AboutUsActivity.class);
                            startActivity(intent);
                            return true;
                        }

                    }
                    return true;
                }
            });


            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                gridLayoutManager = new GridLayoutManager(activity, 2);
            else
                gridLayoutManager = new GridLayoutManager(activity, 1);

            rvJournals.setLayoutManager(gridLayoutManager);


            srLayout.setOnRefreshListener(this);

            adapter = new JournalGridAdapter(getJournals(), this);
            rvJournals.setAdapter(adapter);
            registerForContextMenu(rvJournals);

            adapter.setOnItemClickListener(this);

            journalHelper.initPayments(onPaymentInitListener);


        } catch (Exception e) {
            Log.e(TAG, "", e);
            //activity.finish();
        }
    }

    private List<Journal> getJournals() {
        try {
            return journalHelper.getJournals();
        } catch (JournalException e) {
            Log.e(TAG, "", e);
            return Collections.EMPTY_LIST;
        }
    }

    private void updateJournalsGrid() {
        List<Journal> journals;
        try {
            journals = journalHelper.getJournals();

        } catch (JournalException e) {
            Log.e(TAG, "", e);
            Toast.makeText(activity, R.string.get_journals_failed, Toast.LENGTH_LONG).show();
            journals = Collections.EMPTY_LIST;
        }
        adapter.journals = journals;
        adapter.notifyDataSetChanged();

    }


    private void downloadJournal(Journal journal) {
        journal.isDownloading = true;
        adapter.notifyDataSetChanged();
        (new JournalDownloadTask()).execute(journal);
    }

    private void requestExternalStoragePermission(int requestCode) {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
    }

    private boolean hasExternalStoragePermission() {
        return ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (hasExternalStoragePermission() && lastJournal != null) {
            if (requestCode == EXTERNAL_STORAGE_REQUEST_CODE_FOR_DOWNLOAD) {
                downloadJournal(lastJournal);
            } else if (requestCode == EXTERNAL_STORAGE_REQUEST_CODE_FOR_OPEN) {
                openJournal(lastJournal);
            }
        }
    }

    private void openJournal(Journal journal) {
        Uri uri = Uri.parse(journal.getFileName(activity));
        Intent intent = new Intent(activity, MuPDFActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(uri);
        intent.putExtra("name", journal.getName());
        startActivity(intent);
    }

    private void deleteJournal(Journal journal) {
        try {
            journalHelper.deleteDownloadFile(journal);
        } catch (JournalException e) {
            Log.e(TAG, "", e);
        }
        adapter.notifyDataSetChanged();

    }


    private void updateJournals() {
        Log.e(TAG, "Start update journals");

        (new JournalsUpdateTask()).execute();
    }


    private void updatePayments() {
        Log.e(TAG, "Start updating payments");

        List<Journal> journals = null;
        try {
            journals = journalHelper.getJournals();
        } catch (JournalException e) {
            Log.e(TAG, "", e);
            Toast.makeText(activity, R.string.get_journals_failed, Toast.LENGTH_LONG).show();
            return;
        }

        journalHelper.restorePayment(journals, new OnRestoreListener() {
            @Override
            public void restoreSucceed() {
                Log.e(TAG, "Payments restored");

                updateJournalsGrid();

            }

            @Override
            public void restoreFailed() {
                Log.e(TAG, "Payments restore failed");

                updateJournalsGrid();
            }
        });
    }


    private void buyJournal(Journal journal) {
        journalHelper.buyJournal(activity, journal, new OnPaymentListener() {
            @Override
            public void onPaymentSucceed() {

            }

            @Override
            public void onConsumeSucceed() {
                updateJournalsGrid();
                showToast(activity.getString(R.string.buy_successful));
            }

            @Override
            public void onPaymentFailed() {
                showToast(activity.getString(R.string.buy_failed));
            }

            @Override
            public void onConsumeFailed() {
                showToast(activity.getString(R.string.buy_failed));
            }
        });
    }

    private void showToast(String text) {
        Toast.makeText(activity, text, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onItemClick(Journal journal, int pos) {
        if (journal.isFree() || journal.isAvailable()) {
            lastJournal = journal;

            if (journal.isDownloaded()) {
                if (hasExternalStoragePermission())
                    openJournal(journal);
                else
                    requestExternalStoragePermission(EXTERNAL_STORAGE_REQUEST_CODE_FOR_OPEN);
            } else {
                if (hasExternalStoragePermission())
                    downloadJournal(journal);
                else
                    requestExternalStoragePermission(EXTERNAL_STORAGE_REQUEST_CODE_FOR_DOWNLOAD);
            }

        } else {
            buyJournal(journal);
        }
    }


    @Override
    public void onRefresh() {
        updateJournals();
    }

    @Override
    public void onCardMenuItemClick(Journal journal, int menuItemId) {
        switch (menuItemId) {
            case R.id.open:
                lastJournal = journal;
                if (hasExternalStoragePermission())
                    openJournal(journal);
                else requestExternalStoragePermission(EXTERNAL_STORAGE_REQUEST_CODE_FOR_OPEN);
                break;
            case R.id.buy:
                buyJournal(journal);
                break;
            case R.id.delete:
                deleteJournal(journal);
                break;
            case R.id.download:
                lastJournal = journal;
                if (hasExternalStoragePermission())
                    downloadJournal(journal);
                else requestExternalStoragePermission(EXTERNAL_STORAGE_REQUEST_CODE_FOR_DOWNLOAD);
                break;
        }
    }

    class JournalsUpdateTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                journalHelper.updateJournalsFromServer();
                return true;
            } catch (JournalException e) {
                Log.e(TAG, "", e);
            }
            return false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean res) {
            Log.e(TAG, "Journals updated");

            if (res) {
                updateJournalsGrid();
                updatePayments();
            }
            srLayout.setRefreshing(false);


            super.onPostExecute(res);
        }
    }


    class JournalDownloadTask extends AsyncTask<Journal, Integer, Boolean> {
        Journal journal;

        @Override
        protected Boolean doInBackground(Journal... journals) {
            journal = journals[0];
            return journalHelper.downloadJournal(journal);
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean res) {
            journal.isDownloading = false;
            adapter.notifyDataSetChanged();
            super.onPostExecute(res);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
    }


}
