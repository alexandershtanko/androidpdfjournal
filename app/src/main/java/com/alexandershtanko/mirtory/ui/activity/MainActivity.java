package com.alexandershtanko.mirtory.ui.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.alexandershtanko.mirtory.R;
import com.alexandershtanko.mirtory.helper.JournalHelper;
import com.alexandershtanko.mirtory.ui.fragment.JournalGridFragment;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public JournalHelper journalHelper;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!journalHelper.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        journalHelper = new JournalHelper(this);
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            journalHelper.release();
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    private void initView() {
        selectJournalGridFragment();
    }

    public void selectJournalGridFragment() {
        Fragment fragment = JournalGridFragment.newInstance();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
}
