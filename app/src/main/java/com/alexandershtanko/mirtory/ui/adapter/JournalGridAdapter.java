package com.alexandershtanko.mirtory.ui.adapter;


import android.graphics.PorterDuff;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alexandershtanko.mirtory.R;
import com.alexandershtanko.mirtory.listener.OnCardMenuItemClickListener;
import com.alexandershtanko.mirtory.listener.OnItemClickListener;
import com.alexandershtanko.mirtory.model.Journal;

import java.util.List;

public class JournalGridAdapter extends RecyclerView.Adapter<JournalGridAdapter.ViewHolder> {

    private final OnCardMenuItemClickListener onCardMenuItemClickListener;
    public List<Journal> journals;
    private OnItemClickListener onItemClickListener;


    public JournalGridAdapter(List<Journal> journals, OnCardMenuItemClickListener onCardMenuItemClickListener) {
        this.journals = journals;

        this.onCardMenuItemClickListener = onCardMenuItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public JournalGridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_journal, parent, false);

        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {


        viewHolder.name.setText(viewHolder.itemView.getContext().getString(R.string.number)+" №"+journals.get(position).getName());

        if (journals.get(position).isDownloading) {
            viewHolder.progress.setVisibility(View.VISIBLE);
            viewHolder.action.setVisibility(View.GONE);
        } else {
            viewHolder.progress.setVisibility(View.GONE);
            if (!journals.get(position).isFree() && !journals.get(position).isAvailable()) {
                viewHolder.action.setVisibility(View.VISIBLE);

                viewHolder.action.setText(R.string.buy_journal);
            } else {
                viewHolder.action.setVisibility(View.VISIBLE);

                if (!journals.get(position).isDownloaded())
                    viewHolder.action.setText(R.string.download_journal);
                else
                    viewHolder.action.setText(R.string.open_journal);
            }
        }

        viewHolder.action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = viewHolder.getAdapterPosition();
                if (onItemClickListener != null)
                    onItemClickListener.onItemClick(journals.get(pos), pos);
            }
        });


        viewHolder.itemView.setBackgroundColor(viewHolder.itemView.getContext().getResources().getColor(position % 2 == 0 ? R.color.gray : R.color.gray1));


    }

    @Override
    public int getItemCount() {
        return journals.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView action;
        public TextView name;
        public ProgressBar progress;


        public ViewHolder(final View itemLayoutView) {
            super(itemLayoutView);
            action = (TextView) itemLayoutView.findViewById(R.id.button_action);
            name = (TextView) itemLayoutView
                    .findViewById(R.id.name);



            progress = (ProgressBar) itemLayoutView.findViewById(R.id.progress);
            progress.getIndeterminateDrawable().setColorFilter(itemLayoutView.getResources()
                    .getColor(R.color.primary), PorterDuff.Mode.SRC_IN);

            itemLayoutView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    Journal journal = journals.get(getAdapterPosition());
                    PopupMenu popup = new PopupMenu(v.getContext(), v);

                    popup.inflate(R.menu.popup_menu);
                    popup.getMenu().findItem(R.id.open).setVisible(false);
                    popup.getMenu().findItem(R.id.delete).setVisible(false);
                    popup.getMenu().findItem(R.id.buy).setVisible(false);
                    popup.getMenu().findItem(R.id.download).setVisible(false);

                    if (journal.isDownloaded()) {
                        popup.getMenu().findItem(R.id.open).setVisible(true);
                        popup.getMenu().findItem(R.id.delete).setVisible(true);
                    } else {
                        if (!journal.isAvailable() && !journal.isFree()) {
                            popup.getMenu().findItem(R.id.buy).setVisible(true);
                        } else {
                            popup.getMenu().findItem(R.id.download).setVisible(true);
                        }
                    }

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Journal journal = journals.get(getAdapterPosition());
                            onCardMenuItemClickListener.onCardMenuItemClick(journal, item.getItemId());
                            return false;
                        }
                    });

                    popup.show();

                    return false;
                }
            });


        }

    }

}
