package com.alexandershtanko.mirtory.exception;

/**
 * Created by aleksandr on 31.07.15.
 */
public class ConnectionException extends Exception {
    public ConnectionException(Exception e)
    {
        super(e);
    }
}
