package com.alexandershtanko.mirtory.exception;

/**
 * Created by aleksandr on 31.07.15.
 */
public class JournalException extends Exception {
    public JournalException(Exception exception) {
        super(exception);
    }
}
