package com.alexandershtanko.mirtory.exception;

/**
 * Created by aleksandr on 31.07.15.
 */
public class DbException extends Exception {
    public DbException(Exception e)
    {
        super(e);
    }
}
