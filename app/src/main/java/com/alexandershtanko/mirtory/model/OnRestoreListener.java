package com.alexandershtanko.mirtory.model;

/**
 * Created by aleksandr on 02.08.15.
 */
public interface OnRestoreListener {
    void restoreSucceed();
    void restoreFailed();
}
