package com.alexandershtanko.mirtory.model;

import android.content.Context;
import android.util.Log;

import com.alexandershtanko.mirtory.util.AppUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

/**
 * Created by aleksandr on 29.07.15.
 */
public class Journal extends SugarRecord<Journal> {


    private static final String TAG = "Journal";
    String remoteId;
    String name;
    String dateAdd;
    Boolean isFree;
    Boolean isAvailable = false;
    Boolean isDownloaded = false;
    Float price;

    @JsonIgnore
    @Ignore
    public Boolean isDownloading=false;

    public Journal() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(String dateAdd) {
        this.dateAdd = dateAdd;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setIsFree(boolean isFree) {
        this.isFree = isFree;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void setIsDownloaded(boolean isDownloaded) {
        this.isDownloaded = isDownloaded;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(String remoteId) {
        this.remoteId = remoteId;
    }

    @JsonIgnore
    public String getFileName(Context context) {
       return getFilePath(context)+remoteId+".dd";
    }
    @JsonIgnore
    public String getFilePath(Context context) {
        try {
            return AppUtils.getDataDir(context) + "/";
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return "/sdcard/journals/";

        }
    }

    @JsonIgnore
    public String getSku() {
        return "item" + getRemoteId();
    }
}
