package com.alexandershtanko.mirtory.model;

import com.orm.SugarRecord;

/**
 * Created by aleksandr on 31.07.15.
 */
public class LastSeenPage extends SugarRecord<LastSeenPage> {
    Long journalId;
    Integer page;

    public Long getJournalId() {
        return journalId;
    }

    public void setJournalId(Long journalId) {
        this.journalId = journalId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public LastSeenPage(Long journalId, Integer page) {
        this.journalId = journalId;
        this.page = page;
    }
}
