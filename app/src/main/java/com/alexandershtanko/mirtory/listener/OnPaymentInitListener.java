package com.alexandershtanko.mirtory.listener;

/**
 * Created by aleksandr on 02.08.15.
 */
public interface OnPaymentInitListener {
    void onSuccess();

    void onError(String msg);
}
