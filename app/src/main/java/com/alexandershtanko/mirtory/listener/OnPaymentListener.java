package com.alexandershtanko.mirtory.listener;

/**
 * Created by aleksandr on 02.08.15.
 */
public interface OnPaymentListener {
    void onPaymentSucceed();
    void onConsumeSucceed();
    void onPaymentFailed();
    void onConsumeFailed();
}
