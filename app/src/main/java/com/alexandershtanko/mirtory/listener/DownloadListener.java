package com.alexandershtanko.mirtory.listener;

/**
 * Created by aleksandr on 31.07.15.
 */
public interface DownloadListener {
    void onDownloadStarted();
    void onDownloadFailed();
    void onDownloadCompleted();
    void onDownloadProgress(int progress);
}
