package com.alexandershtanko.mirtory.listener;

import com.alexandershtanko.mirtory.model.Journal;

/**
 * Created by aleksandr on 04.08.15.
 */
public interface OnItemClickListener {
    void onItemClick(Journal item, int pos);
}
