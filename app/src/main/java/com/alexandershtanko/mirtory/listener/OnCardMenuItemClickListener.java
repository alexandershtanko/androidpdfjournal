package com.alexandershtanko.mirtory.listener;

import com.alexandershtanko.mirtory.model.Journal;

/**
 * Created by alexander on 8/11/15.
 */
public interface OnCardMenuItemClickListener {
    void onCardMenuItemClick(Journal journal,int menuItemId);
}
