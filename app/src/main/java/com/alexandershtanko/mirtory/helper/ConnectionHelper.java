package com.alexandershtanko.mirtory.helper;

import android.content.Context;

import com.alexandershtanko.mirtory.exception.ConnectionException;
import com.alexandershtanko.mirtory.model.Journal;
import com.alexandershtanko.mirtory.util.HttpUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aleksandr on 30.07.15.
 */
class ConnectionHelper {
    private static final String API_URL = "http://mirtory.com/android/ajax.php";
    private static final String REQUEST_TYPE = "type";
    private static final String REQUEST_TYPE_JOURNALS = "journals";
    private static final String REQUEST_TYPE_DOWNLOAD = "download";

    private final Context context;

    public ConnectionHelper(Context context) {
        this.context = context;
    }

    public List<Journal> getJournals() throws ConnectionException {
        try {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(REQUEST_TYPE, REQUEST_TYPE_JOURNALS);

            List<Map> list = HttpUtils.makeRequest(API_URL, parameters);

            List<Journal> journals = new ArrayList<>();

            if (list != null && list.size() > 0)
            {
                for(Map<String,Object> map:list)
                {
                    Journal journal=new Journal();
                    journal.setName((String) map.get("name"));
                    journal.setDateAdd((String) map.get("dateAdd"));
                    journal.setRemoteId((String) map.get("remoteId"));
                    journal.setPrice(Float.parseFloat((String) map.get("price")));
                    journal.setIsFree((Boolean) map.get("isFree"));
                    journals.add(journal);
                }
            }

            return journals;

        } catch (Exception e) {
            throw new ConnectionException(e);
        }
    }


    public boolean downloadJournal(Journal journal) {
        try {

            String remoteId = journal.getRemoteId();
            File dir=new File(journal.getFilePath(context));
            if(!dir.exists())
                dir.mkdirs();

            File outputFile = new File(journal.getFileName(context));
            if(!outputFile.exists())
                outputFile.createNewFile();

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(REQUEST_TYPE, REQUEST_TYPE_DOWNLOAD);
            parameters.put("remoteId", remoteId);

            HttpUtils.downloadFile(API_URL, parameters, outputFile);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}


