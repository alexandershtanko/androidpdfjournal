package com.alexandershtanko.mirtory.helper;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import com.alexandershtanko.mirtory.exception.DbException;
import com.alexandershtanko.mirtory.model.Journal;
import com.alexandershtanko.mirtory.model.LastSeenPage;

import java.util.List;

/**
 * Created by aleksandr on 31.07.15.
 */
class DbHelper {
    private final Context context;

    public DbHelper(Context context) {
        this.context = context;
    }

    public List<Journal> getJournals() throws DbException {
        try {
            return Journal.find(Journal.class, null, new String[]{}, null, "date_Add desc", null);
        } catch (SQLiteException e) {
            throw new DbException(e);
        }


    }

    public List<Journal> getAvailableJournals() throws DbException {
        try {
            return Journal.find(Journal.class, "is_Available = true", null, null, "date_Add desc", null);
        } catch (SQLiteException e) {
            throw new DbException(e);
        }
    }

    public void updateLastSeenPage(Long journalId, int page) throws DbException {
        try {
            LastSeenPage.deleteAll(LastSeenPage.class, "remote_Id = ?" + journalId.toString(), null);
            LastSeenPage lastSeenPage = new LastSeenPage(journalId, page);
            lastSeenPage.save();
        } catch (SQLiteException e) {
            throw new DbException(e);
        }

    }

    public int getLastSeenPage(Long journalId) throws DbException {
        try {
            List<LastSeenPage> lastSeenPageList = LastSeenPage.find(LastSeenPage.class, "remote_Id = " + journalId.toString(), null, null, null, "1");
            if (lastSeenPageList != null && lastSeenPageList.size() == 1)
                return lastSeenPageList.get(0).getPage();
            return 0;
        } catch (SQLiteException e) {
            throw new DbException(e);
        }
    }

    public void updateJournal(Journal journal) throws DbException {
        try {
            journal.save();
        } catch (SQLiteException e) {
            throw new DbException(e);
        }
    }


    public void addJournalsIfNotExist(List<Journal> journals) throws DbException {
        try {
            for (Journal journal : journals) {
                Journal savedJournal = getJournalByRemoteId(journal.getRemoteId());
                if (savedJournal != null) {
                    savedJournal.setIsFree(journal.isFree());
                    savedJournal.setName(journal.getName());
                    savedJournal.save();
                } else
                    journal.save();
            }
        } catch (SQLiteException e) {
            throw new DbException(e);
        }

    }

    private long getCount(Journal journal) {
        try {
            return Journal.count(Journal.class, "remote_Id = '" + journal.getRemoteId() + "'", null);
        } catch (SQLiteException e) {
            return 0;
        }
    }

    public Journal getJournalByRemoteId(String remoteId) {
        try {
            List<Journal> journal = Journal.find(Journal.class, "remote_Id = '" + remoteId + "'", null);
            if (journal.size() > 0)
                return journal.get(0);
        } catch (Exception ignored) {
        }
        return null;
    }

    public Journal getJournal(Long id) throws DbException {
        try {
            return Journal.findById(Journal.class, id);
        } catch (SQLiteException e) {
            throw new DbException(e);
        }
    }
}
