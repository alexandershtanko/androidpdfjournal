package com.alexandershtanko.mirtory.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.alexandershtanko.mirtory.util.billing.IabHelper;
import com.alexandershtanko.mirtory.util.billing.IabResult;
import com.alexandershtanko.mirtory.util.billing.Inventory;
import com.alexandershtanko.mirtory.util.billing.Purchase;

import java.util.List;

/**
 * Created by aleksandr on 30.07.15.
 */
class PaymentHelper {
    private static final String TAG = "BillingHelper";
    private static int ON_ACTIVITY_RESULT_REQUEST_CODE = 10001;
    private final Context context;
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgIK5Mp8rC59Ceiatn2IloMb7t5N7Dq6FbGTK37AEHbIaH2Rt3WArX0WQ9mKuRLDt0WQfTKvxUpAn7zwvixFI7M0qaL5ohLT198RluSms7Zly9ox5dzTZMbiVOsTtJMi432v5ebq1Z/gaz+HRLmzQFzrvlsmK49rJQgJGWWBRXGGEZ8Q/zMwJfX4p2qIh6Xgr2NbutPW/uqXWX2RD8/k9u/IXgzrbqxiRa2nF+CzmKnFfNlQ/Uh6/v4l+ZzO9hhvs3ptcIFbp9OqgHS0KOisZiKbjkr8ov9Ahdz2qnJvUrTyIB4SOsScLF4u01MkqdJCNGII0c6Dw3uimgPqNkiks/wIDAQAB";
    private IabHelper mHelper;

    public PaymentHelper(Context context) {
        this.context = context;
    }


    public void init(final OnInitListener onInitListener) {
        try {
            mHelper = new IabHelper(context, base64EncodedPublicKey);
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                public void onIabSetupFinished(IabResult result) {
                    if (result.isFailure()) {
                        onInitListener.onError(result.getMessage());
                        Log.d(TAG, "In-app Billing setup failed: " +
                                result);
                    } else {
                        onInitListener.onSuccess();
                        Log.d(TAG, "In-app Billing is set up OK");
                    }
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    public void release() {
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    public void purchaseItem(Activity activity, String sku, String token, final OnPurchaseFinishedListener onPurchaseFinishedListener) {
        try {
            IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
                @Override
                public void onIabPurchaseFinished(IabResult result, Purchase info) {
                    if (result.isFailure())
                        onPurchaseFinishedListener.onError(info, result.getMessage());
                    else
                        onPurchaseFinishedListener.onSuccess(info);

                }
            };

            mHelper.launchPurchaseFlow(activity, sku, ON_ACTIVITY_RESULT_REQUEST_CODE,
                    mPurchaseFinishedListener, token);
        }
        catch (Exception ignored){}
    }

    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        return mHelper.handleActivityResult(ON_ACTIVITY_RESULT_REQUEST_CODE,
                resultCode, data);
    }


    public void checkItems(final List<String> skuList, final OnCheckResultListener onCheckResultListener) {


        IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
                = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result,
                                                 Inventory inventory) {

                if (result.isFailure()) {
                    onCheckResultListener.onError(result.getMessage());
                } else {
                    for (String sku : skuList) {
                        Purchase purchase = inventory.getPurchase(sku);
                        if (purchase != null)
                            onCheckResultListener.onSuccess(purchase);
                        else onCheckResultListener.onError("");
                    }

                }
            }
        };

        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    public void checkItem(final String sku, final OnCheckResultListener onCheckResultListener) {

        IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
                = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result,
                                                 Inventory inventory) {

                if (result.isFailure()) {
                    onCheckResultListener.onError(result.getMessage());
                } else {
                    Purchase purchase = inventory.getPurchase(sku);
                    if (purchase == null)
                        onCheckResultListener.onError("Unable to find purchase");
                    else
                        onCheckResultListener.onSuccess(purchase);
                }
            }
        };
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }


    public interface OnInitListener {
        void onSuccess();

        void onError(String msg);
    }

    public interface OnPurchaseFinishedListener {
        void onSuccess(Purchase info);

        void onError(Purchase info, String msg);
    }

    public interface OnCheckResultListener {
        void onSuccess(Purchase info);

        void onError(String msg);
    }
}
