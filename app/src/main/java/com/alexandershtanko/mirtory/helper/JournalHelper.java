package com.alexandershtanko.mirtory.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.alexandershtanko.mirtory.exception.ConnectionException;
import com.alexandershtanko.mirtory.exception.DbException;
import com.alexandershtanko.mirtory.exception.JournalException;
import com.alexandershtanko.mirtory.listener.OnPaymentInitListener;
import com.alexandershtanko.mirtory.listener.OnPaymentListener;
import com.alexandershtanko.mirtory.model.Journal;
import com.alexandershtanko.mirtory.model.OnRestoreListener;
import com.alexandershtanko.mirtory.model.PaymentConstants;
import com.alexandershtanko.mirtory.util.billing.Purchase;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksandr on 30.07.15.
 */
public class JournalHelper {
    private static final String TAG = "JournalHelper";
    private ConnectionHelper connectionHelper;
    private DbHelper dbHelper;
    private PaymentHelper paymentHelper;
    private Context context;

    public JournalHelper(Context context) {
        this.context = context;
        connectionHelper = new ConnectionHelper(context);
        dbHelper = new DbHelper(context);
        paymentHelper = new PaymentHelper(context);
    }

    public void initPayments(final OnPaymentInitListener onPaymentInitListener) {
        paymentHelper.init(new PaymentHelper.OnInitListener() {
            @Override
            public void onSuccess() {
                onPaymentInitListener.onSuccess();
            }

            @Override
            public void onError(String msg) {
                onPaymentInitListener.onError(msg);
                Log.e(TAG, msg);
            }
        });
    }

    //Получение журналов из БД
    public List<Journal> getJournals() throws JournalException {
        try {
            return dbHelper.getJournals();
        } catch (DbException e) {
            throw new JournalException(e);
        }
    }

    //Обновление списка журналов в БД через интернет
    public void updateJournalsFromServer() throws JournalException {
        try {
            List<Journal> journals = connectionHelper.getJournals();
            dbHelper.addJournalsIfNotExist(journals);
        } catch (ConnectionException e) {
            throw new JournalException(e);
        } catch (DbException e) {
            throw new JournalException(e);
        }
    }

    //Сохранение последней прочитанной страницы для журнала
    public void updateLastSeenPage(Journal journal, int page) throws JournalException {
        try {
            dbHelper.updateLastSeenPage(journal.getId(), page);
        } catch (DbException e) {
            throw new JournalException(e);
        }
    }

    //Получение последней прочитанной страницы журнала
    public int getLastSeenPage(Journal journal) {
        try {
            return dbHelper.getLastSeenPage(journal.getId());
        } catch (DbException e) {
            return 0;
        }
    }

    //Скачивание журнала
    public boolean downloadJournal(Journal journal) {
        boolean res = connectionHelper.downloadJournal(journal);
        if (res) {
            journal.setIsDownloaded(true);
            try {
                dbHelper.updateJournal(journal);
            } catch (DbException e) {
                res = false;
            }
        }
        return res;
    }


    //Покупка журнала
    public void buyJournal(final Activity activity, final Journal journal, final OnPaymentListener onPaymentListener) {

        final PaymentHelper.OnCheckResultListener onCheckResultListener = new PaymentHelper.OnCheckResultListener() {
            @Override
            public void onSuccess(Purchase info) {
                journal.setIsAvailable(true);
                try {
                    updateJournal(journal);
                } catch (JournalException e) {
                    Log.e(TAG, "", e);
                    onPaymentListener.onConsumeFailed();
                    return;
                }
                onPaymentListener.onConsumeSucceed();
            }

            @Override
            public void onError(String msg) {
                onPaymentListener.onConsumeFailed();
            }
        };

        PaymentHelper.OnPurchaseFinishedListener onPurchaseFinishedListener = new PaymentHelper.OnPurchaseFinishedListener() {
            @Override
            public void onSuccess(Purchase info) {
                onPaymentListener.onPaymentSucceed();
                paymentHelper.checkItem(journal.getSku(), onCheckResultListener);
            }

            @Override
            public void onError(Purchase info, String msg) {
                onPaymentListener.onPaymentFailed();
            }
        };


        if (!journal.isAvailable() && !journal.isFree()) {
            paymentHelper.purchaseItem(activity, journal.getSku(), PaymentConstants.PURCHASE_TOKEN, onPurchaseFinishedListener);
        }
    }

    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        return paymentHelper.handleActivityResult(requestCode, resultCode, data);
    }

    public void release() {
        paymentHelper.release();
    }


    private int restoredCount = 0;

    public void restorePayment(final List<Journal> journals, final OnRestoreListener onRestoreListener) {
        final PaymentHelper.OnCheckResultListener onCheckResultListener = new PaymentHelper.OnCheckResultListener() {
            @Override
            public void onSuccess(Purchase info) {
                for (Journal journal : journals) {
                    if (journal.getSku().equals(info.getSku())) {
                        journal.setIsAvailable(true);
                        try {
                            updateJournal(journal);
                        } catch (JournalException e) {
                            Log.e(TAG, "", e);
                        }
                        restoredCount--;
                        if (restoredCount == 0)
                            onRestoreListener.restoreSucceed();
                        return;
                    }
                }
            }

            @Override
            public void onError(String msg) {
                restoredCount--;
                if (restoredCount == 0)
                    onRestoreListener.restoreSucceed();
            }
        };

        List<String> skuList = new ArrayList<>();
        for (Journal journal : journals) {
            if (!journal.isFree() && !journal.isAvailable()) {
                restoredCount++;
                skuList.add(journal.getSku());
            }
        }

        if (skuList.size() == 0)
            onRestoreListener.restoreSucceed();
        else
            paymentHelper.checkItems(skuList, onCheckResultListener);
    }

    private void updateJournal(Journal journal) throws JournalException {
        try {
            dbHelper.updateJournal(journal);
        } catch (DbException e) {
            throw new JournalException(e);
        }
    }


    public Journal getJournal(Long id) throws JournalException {
        try {
            return dbHelper.getJournal(id);
        } catch (DbException e) {
            throw new JournalException(e);
        }
    }


    public void deleteDownloadFile(Journal journal) throws JournalException {
        journal.setIsDownloaded(false);
        try {
            File file = new File(journal.getFileName(context));
            if (file.exists())
                file.delete();
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        updateJournal(journal);
    }
}
