LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

MY_ROOT := ../../mupdf

ifeq ($(TARGET_ARCH),arm)
LOCAL_CFLAGS += -DARCH_ARM -DARCH_THUMB -DARCH_ARM_CAN_LOAD_UNALIGNED
ifdef NDK_PROFILER
LOCAL_CFLAGS += -pg -DNDK_PROFILER
endif
endif
LOCAL_CFLAGS += -DAA_BITS=8
ifdef MEMENTO
LOCAL_CFLAGS += -DMEMENTO -DMEMENTO_LEAKONLY
endif
ifdef SSL_BUILD
LOCAL_CFLAGS += -DHAVE_OPENSSL
endif

LOCAL_C_INCLUDES := \
	../../mupdf/thirdparty/jbig2dec \
	../../mupdf/thirdparty/openjpeg/libopenjpeg \
	../../mupdf/thirdparty/jpeg \
	../../mupdf/thirdparty/mujs \
	../../mupdf/thirdparty/zlib \
	../../mupdf/thirdparty/freetype/include \
	../../mupdf/source/fitz \
	../../mupdf/source/pdf \
	../../mupdf/source/xps \
	../../mupdf/source/cbz \
	../../mupdf/source/img \
	../../mupdf/source/tiff \
	../../mupdf/scripts/freetype \
	../../mupdf/scripts/jpeg \
	../../mupdf/scripts/openjpeg \
	../../mupdf/generated \
	../../mupdf/resources \
	../../mupdf/include \
	../../mupdf
ifdef V8_BUILD
LOCAL_C_INCLUDES += ../../mupdf/thirdparty/$(V8)/include
endif
ifdef SSL_BUILD
LOCAL_C_INCLUDES += ../../mupdf/thirdparty/openssl/include
endif

LOCAL_MODULE    := mupdfcore
LOCAL_SRC_FILES := \
	$(wildcard $(MY_ROOT)/source/fitz/*.c) \
	$(wildcard $(MY_ROOT)/source/pdf/*.c) \
	$(wildcard $(MY_ROOT)/source/xps/*.c) \
	$(wildcard $(MY_ROOT)/source/cbz/*.c) \
	$(wildcard $(MY_ROOT)/source/html/*.c)
LOCAL_SRC_FILES += \
	$(MY_ROOT)/source/pdf/js/pdf-js.c \
	$(MY_ROOT)/source/pdf/js/pdf-jsimp-mu.c

LOCAL_LDLIBS    := -lm -llog -ljnigraphics


include $(BUILD_STATIC_LIBRARY)
