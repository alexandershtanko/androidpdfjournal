<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);

header('Access-Control-Allow-Headers:Origin, X-Requested-With, Content-Type, Accept,Access-Control-Allow-Headers');
header('Access-Control-Allow-Origin: *');

  function file_force_download($file) {
			  if (file_exists($file)) {
			    // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
			    // если этого не сделать файл будет читаться в память полностью!
			    if (ob_get_level()) {
			      ob_end_clean();
			    }
			    // заставляем браузер показать окно сохранения файла
			    header('Content-Description: File Transfer');
			    header('Content-Type: application/octet-stream');
			    header('Content-Disposition: attachment; filename=' . basename($file));
			    header('Content-Transfer-Encoding: binary');
			    header('Expires: 0');
			    header('Cache-Control: must-revalidate');
			    header('Pragma: public');
			    header('Content-Length: ' . filesize($file));
			    // читаем файл и отправляем его пользователю
			    readfile($file);
			    exit;
			  }
			  echo("file not found:".$file);
			}

if (isset($_GET['type'])) {


    include './db.php';
    $db = new database();

    $type = $_GET['type'];
    $res = "";
    $journals=array();
    if ($type == "journals") { 
        $res = $db->GetJournals();    
	
	if($res!=null)
	{
	 foreach($res as $r)
	 {
	   $journal=array();
	   $journal["remoteId"]=$r["m_id"];
	   $journal["name"]=$r["name"];
	   $journal["dateAdd"]=$r["date_add"];
	   $journal["isFree"]=($r["is_free"]==1);
	   $journal["price"]=$r["price"];
	   $journals[]=$journal;
	 }
	}
	echo json_encode($journals);   
    }  
    if($type == "download")
    {
      if(isset($_GET['remoteId']))
      {
        $remoteId=$_GET['remoteId'];
	$url=$db->GetJournalUrl($remoteId);
	if($url!=null)
	  file_force_download($url);
	else
	{
		header("HTTP/1.0 404 Not Found");
		echo "<h1>404 Not Found</h1>";
		echo "The page that you have requested could not be found.";
	}
		
      }	
    }

}

?>
